# Boilerplate Docker: php-mysql-phpmydamin

## Install

Clone project :

```bash
git clone git@gitlab.com:phil-all/boilerplate-docker-php-mysql-phpmyadmin.git
```

Launch docker :

```bash
docker-compose build && \
docker-compose up -d
```

## Use

### Web server

Web server available on : `127.0.0.1:8888`.

Place app entry point `index.*` in folder : `./app/`.

PHP Dockerfile have to be completed with project needed extension.

### Phpmyadmin

Phpmyadmin available on : `127.0.0.1:8800`.
